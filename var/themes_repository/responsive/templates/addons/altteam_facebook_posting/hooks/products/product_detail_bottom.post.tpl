v{assign var="app_id" value=fn_get_facebook_app_id()}
{assign var="current_facebook_url" value=fn_url("products.view?product_id=`$product.product_id`")}
{assign var="facebook_api_desc" value=__("facebook_api_description")}
{include file="buttons/button.tpl" but_text=__("facebook_api_button") but_href="products.view?product_id=`$product.product_id`" but_id="facebook_api_{$product.product_id}_{$product.product_id}" but_role="text" but_name="" but_meta="cm-ajax facebookPost"}
<style>
    .facebookPost{
        background-color: #1877f2;
        color:white;
        font-size:12px;
    }
</style>
<script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>
<script>
    (function(_, $){
            window.fbAsyncInit = function() {
        FB.init({
        appId            : "{$app_id}",
        autoLogAppEvents : true,
        xfbml            : true,
        version          : 'v6.0'
        });
                };
        $("#facebook_api_{$product.product_id}_{$product.product_id}").click(() => {
                FB.ui({
                    method: 'feed',
                    link: "{$current_facebook_url}",
                },
                function(response) {
                    if (response && !response.error_message) {
                        $.ceAjax('request', fn_url("products.fb_post"), {
                                method: 'get',
                                data: {
                                    fb_posting: "Y",
                                },
                            });
                    } else {
                        $.ceAjax('request', fn_url("products.fb_post"), {
                            method: 'get',
                            data: {
                                fb_posting: "N",
                            },
                        });
                    }
                } 
            );
        })
    })(Tygh,Tygh.$)
</script>