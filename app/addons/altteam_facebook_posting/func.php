<?php

/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use  
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @license    http://www.alt-team.com/addons-license-agreement.html
 ****************************************************************************/


use Tygh\Registry;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}


function fn_posting_facebook($promotion_id, $promotion, &$auth)
{
    if (!isset($auth['fb_is_posted']) || $auth['fb_is_posted'] != 'Y') {
        if (!empty($auth['fb_posting']) && $auth['fb_posting'] == 'Y') {
            $auth['fb_is_posted'] = 'Y';
            return true;
        }
    }
}

function fn_get_facebook_app_id()
{
    return trim(Registry::get('addons.altteam_facebook_posting.app_id'));
}

function fn_altteam_facebook_posting_checkout_place_orders_pre_route($cart, &$auth, $params)
{
    if(!empty($cart['applied_promotions'])){
        foreach($cart['applied_promotions'] as $promotions){
            $promotions['is_facebook'] = explode(';',$promotions['conditions_hash']);
            foreach ($promotions['is_facebook'] as $value) {
                if($value == "facebook_posting=Y"){
                    $auth['fb_posting'] = 'N';
                    $auth['fb_is_posted'] = 'N';
                }
            }
        }    
    }
}
