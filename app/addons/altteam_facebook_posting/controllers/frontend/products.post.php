<?php

/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use  
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @license    http://www.alt-team.com/addons-license-agreement.html
 ****************************************************************************/

use Tygh\Registry;
use Tygh\Tygh;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    return;
}

if ($mode == 'fb_post') {
    if ($_REQUEST['fb_posting'] == 'Y') {

        Tygh::$app['session']['auth']['fb_posting'] = 'Y';
        fn_set_notification('N', __('notice'), __('facebook_api_posted_success'));
    } else {
        Tygh::$app['session']['auth']['fb_posting'] = 'N';
        fn_set_notification('W', __('warning'), __('facebook_api_posted_error'));
    }
    exit();
}
