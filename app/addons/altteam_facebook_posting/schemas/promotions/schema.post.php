<?php


$schema['conditions']['facebook_posting'] = array(
    'type' => 'statement',
    'field_function' => array('fn_posting_facebook', '#id', '#this', '@auth'),
    'zones' => array('cart'),
);

return $schema;